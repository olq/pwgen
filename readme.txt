Compiling:
  g++-7 -std=c++17 pwgen2017.cpp cxxopts.hpp -o pwgen

Usage:
  ./pwgen [OPTION...]

  -h, --help             Print help (default: false)
  -d, --dict arg         Select dictionary
  -w, --words arg        Number of words in generated password
  -e, --delimiter arg    Specify delimiter between words
  -r, --randomdelimiter  Add random characters as delimiter (default: false)
  -c, --capitalize       Capitalize every word (default: false)
  -s, --scramble         Add a number of random characters to words (default:
                         false)
  -j, --junk arg         Like --scramble but with fixed number of characters
                         across the whole password.
  -n, --numbers arg      Add a random number < n

 Dictionaries options:
  -i, --import arg  Import and clean OpenOffice dictionary file (E.g.
                    language.dic). Default output: language.txt . Download dic files
                    from https://github.com/titoBouzout/Dictionaries
  -o, --output arg  If -i flag is used, specifies output file name for
                    converted dictionary file