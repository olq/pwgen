#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <random>
#include <ctype.h>
#include <iterator>
#include "cxxopts.hpp"

using namespace std;

// TODO
// Make super sure the random generator is good
// Use system dictionary, or maybe dictionaries from installed firefox and crome.
// Calculate complexity of generated passwords.
// Auto clean dic files.


// cleans .dic files (libreoffice or firefox dictionaries, don't remember which)
void clean(vector<string> & words)
{
  for(auto it : words){
    it.erase(std::find(it.begin(), it.end(), '/'), it.end());
  }
}

vector<string> read_words(const string & infile)
{

}


int rnd_pos(const string & word){
  std::random_device r;
  mt19937 gen {random_device{}() }; 
  gen.seed(random_device{}());
  int pos = gen() % word.size();
  return pos;
}

string rnd_char(){
  string del = "!#€%&/()=?@$[]<>,.-;:_1234567890";
  int pos = rnd_pos(del); 
  string tmp = del.substr(pos, 1);
//  string tmp(del[pos], 4);

  // cout << "[" << tmp << "]" << "(" << pos << ")" << endl;
  
  return tmp;
}

// string generate(dictionary, word_count, delimiter, randomdelimiter, capitalize, scramble);
string generate(const vector<string> & words, int word_count, const string & delimiter, bool randomdelimiter, bool capitalize,  bool scramble, int numbers, int junk)
{
  std::random_device r;
  mt19937 gen {random_device{}() };
  
  string pw{};

  for (int i = 0; i < word_count; ++i)
  {
    
    gen.seed(random_device{}());
    int gen_ = gen() % words.size();
    string tempword = words.at( gen_ );
    //cout << "tempword1: " << tempword << endl;

    if (capitalize)
    {
      tempword.at(0) = toupper(tempword[0]);
    }
    

    if (scramble)
    {
      //cout << tempword << " " << gen() % (tempword.size()) << endl;
      gen.seed(random_device{}());
      int n_scrambled_chars = (gen() % tempword.size()) / 2; // no more than half the word will be scrambled. Completely arbitrary limit.
      cout << "adding " << n_scrambled_chars << " extra chars to the word " << tempword << " " << endl;
      for(int j = 0; j < n_scrambled_chars; j++) {  
        tempword.insert(rnd_pos(tempword), rnd_char() ) ;
      }
      cout << tempword << endl;
    }

    
    //cout << "tempword2: " << tempword << endl;
    if (i == word_count - 1 ) {
      pw.append( tempword );
      //pw.append( tempword );
    }
    else {
      if (randomdelimiter) {
        pw.append( tempword + rnd_char());
      }
      else {
        pw.append( tempword + delimiter);
        //pw.append( tempword +  del.at(rnd_pos(del)));
      }
    }
  }

  //should probably insert at random pos instead of the end
  if(numbers > 0)
  {
    gen.seed(random_device{}());
    //gen_ = gen() % words.size();
    //cout << "slump: " << to_string(gen() % numbers) << endl;
    pw.append(delimiter + to_string(gen() % numbers));
  }

  if(junk > 0) {  
    for(int i = 0; i < junk; i++) {  
      pw.insert(rnd_pos(pw), rnd_char() ) ;
    }
  }

  return pw;

}


int main(int argc, char* argv[])
{
  string wordlist_path {};
  string import_path {};
  string output_path {};
  int word_count = 1;
  bool randomdelimiter = false;
  bool capitalize = false;
  bool scramble = false;
  int junk = 0;
  int numbers = 0;
  string delimiter = " ";
  string del = "!#€%&/()=?*_";


  try
  {
    cxxopts::Options options(argv[0], "pwgen - a password generator using dictionaries");
    options
      .positional_help("[optional args]")
      .show_positional_help();

    options.add_options()
      ("h,help", "Print help") 
      ("d,dict", "Select dictionary", cxxopts::value<string>(wordlist_path))
      ("w,words", "Number of words in generated password", cxxopts::value<int>(word_count))
      ("e,delimiter", "Specify delimiter between words", cxxopts::value<string>(delimiter))
      ("r,randomdelimiter", "Add random characters as delimiter")
      ("c,capitalize", "Capitalize every word")
      ("s,scramble", "Add a number of random characters to words")
      ("j,junk", "Like --scramble but with fixed number of characters across the whole password.", cxxopts::value<int>(junk))
      ("n,numbers", "Add a random number < n", cxxopts::value<int>(numbers))
      ;
      options.add_options("Dictionaries")
      ("i,import", "Import and clean OpenOffice dictionary file (E.g. language.dic). Default output: language.txt . Download dic files from https://github.com/titoBouzout/Dictionaries", cxxopts::value<std::string>(import_path))
      ("o,output", "If -i flag is used, specifies output file name for converted dictionary file", cxxopts::value<std::string>(output_path))
      ;
      

      /*
      ("d,dict", "Select dictionary", cxxopts::value<string>(wordlist_path))
      ("w,words", "Number of words in generated password", cxxopts::value<int>(word_count))
      ("r,randomdelimiter", "Add random characters as delimiter", cxxopts::value<bool>(randomdelimiter))
      ("c,capitalize", "Capitalize every word", cxxopts::value<bool>(capitalize))
      ("x,scramble", "Add a number of random characters to words. ", cxxopts::value<int>(scramble))
      ("e,delimiter", "Specify delimiter between words", cxxopts::value<string>(delimiter))
      ("i,import", "[file.dic] Import and clean OpenOffice dictionary file. Default output: file.txt . https://github.com/titoBouzout/Dictionaries", cxxopts::value<std::string>(import_path))
      ("o,output", "If -i flag is used, specifies output file name for converted dictionary file", cxxopts::value<std::string>(output_path))
      ("h,help", "Print help") ;
      */



    // options.add_options("Group")
    //   ("c,compile", "compile")
    //   ("d,drop", "drop", cxxopts::value<std::vector<std::string>>());

    // options.parse_positional({"input", "output", "positional"});

    auto result = options.parse(argc, argv);

    if (result.count("help"))
    {
      std::cout << options.help({"", "Dictionaries"}) << std::endl;
      exit(0);
    }

    if (result.count("dict"))
    {
      wordlist_path = result["dict"].as<std::string>();
     // std::cout << "wordlist_path: " << wordlist_path << std::endl;


      ifstream ifs{wordlist_path};
      vector<string> dictionary {};
      copy(istream_iterator<string>(ifs), istream_iterator<string>(), back_inserter(dictionary) );
      ifs.close();

      // cout << "read " << wordlist_path << " containing " << dictionary.size() << " words" << endl;
    

      if (result.count("words"))
      {
        word_count = result["words"].as<int>();
        // std::cout << "words: " << word_count << std::endl;
      }

      if (result.count("delimiter"))
      {
        delimiter = result["delimiter"].as<string>();
        // std::cout << "delimiter: \"" << delimiter << "\"" << std::endl;
      }

      if (result.count("randomdelimiter"))
      {
        randomdelimiter = true;
        // std::cout << "randomdelimiter: " << randomdelimiter << std::endl;
      }

      if (result.count("capitalize"))
      {
        capitalize = true;
        // std::cout << "capitalize: " << capitalize << std::endl;
      }
      if (result.count("scramble"))
      {
        scramble = true;
        // std::cout << "scramble: " << scramble << std::endl;
      }
      if (result.count("junk"))
      {
        junk = result["junk"].as<int>();;
        // std::cout << "junk: " << junk << std::endl;
      }
      if (result.count("numbers"))
      {
        numbers = result["numbers"].as<int>();;
        // std::cout << "numbers: " << numbers << std::endl;
      }
      
      string pw = generate(dictionary, word_count, delimiter, randomdelimiter, capitalize, scramble, numbers, junk);
      cout << pw << endl;

    }

    else if (result.count("import"))
    {
      import_path = result["import"].as<string>();
      // std::cout << "import: " << import_path << std::endl;
    

      if (result.count("output"))
      {
        output_path = result["output"].as<string>();
        std::cout << "output: " << output_path << std::endl;
      }
    }
    else
    {
      std::cout << options.help({"", "Dictionaries"}) << std::endl;
      exit(0);
    }

  } 
  catch (const cxxopts::OptionException& e)
  {
    std::cout << "error parsing options: " << e.what() << std::endl;
    exit(1);
  }

  return 0;
}